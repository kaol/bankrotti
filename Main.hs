{-# LANGUAGE LambdaCase #-}

module Main where

import Control.Monad
import Data.ByteString.Lazy (putStr, writeFile)
import Data.Default
import Text.XML.HXT.Core hiding (when)
import Hledger (readJournalFile)
import Options.Applicative

import Bankrotti.AccountScheme
import Bankrotti.BalanceSheet
import Bankrotti.CSV
import Bankrotti.GeneralLedger
import Bankrotti.IncomeStatement
import Bankrotti.Receipts

data BankrottiOptions = BankrottiOptions
  { accountSchemeFile :: FilePath
  , journalFile :: FilePath
  , targetDir :: FilePath
  }

bankrottiOptionsParser :: Parser BankrottiOptions
bankrottiOptionsParser =
  BankrottiOptions
  <$> strArgument (metavar "SCHEME")
  <*> strArgument (metavar "JOURNAL")
  <*> strArgument (metavar "DIR")

main :: IO ()
main = do
  bankrotti =<< execParser opts
  where
    opts = info (bankrottiOptionsParser <**> helper) (fullDesc <> progDesc "Bankrotti")

bankrotti :: BankrottiOptions -> IO ()
bankrotti cfg = do
  scheme <- Bankrotti.AccountScheme.readFile $ accountSchemeFile cfg
  readJournalFile def (journalFile cfg) >>= \case
    Left err -> print err
    Right journal ->
      let ledger = buildLedgers scheme journal
          ledgerSumsOnly = (fmap . fmap . fmap) postingAmount ledger
          sheet = balanceSheet ledgerSumsOnly
          incomeStatement = buildIncomeStatement ledgerSumsOnly
          journal' = buildJournal scheme journal
          fullJournal = buildFullJournal scheme journal
          sheetPostings = filterSheetPostings fullJournal
          dir = targetDir cfg <> "/"
      in do
        Data.ByteString.Lazy.writeFile (dir <> "tase.csv") $
          balanceSheetCSV sheet
        when (not $ null sheetPostings) $
          Data.ByteString.Lazy.writeFile (dir <> "tase-liite.csv") $
          balanceSheetExpandedCSV sheetPostings sheet
        Data.ByteString.Lazy.writeFile (dir <> "tuloslaskelma.csv") $
          incomeStatementCSV incomeStatement
        Data.ByteString.Lazy.writeFile (dir <> "päiväkirja-lyhyt.csv") $
          journalCSV journal'
        Data.ByteString.Lazy.writeFile (dir <> "päiväkirja.csv") $
          fullJournalCSV fullJournal
        Data.ByteString.Lazy.writeFile (dir <> "tilitapahtumat.csv") $
          cumulativeCSV journal'
        Data.ByteString.Lazy.writeFile (dir <> "pääkirja.csv") $
          generalLedgerCSV ledger
        generateReceipts "kuitit/" dir journal
