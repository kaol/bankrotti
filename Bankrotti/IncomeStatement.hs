module Bankrotti.IncomeStatement where

import Bankrotti.AccountScheme
import Bankrotti.GeneralLedger

type IncomeStatement n = AccountScheme (Account, n)

buildIncomeStatement :: Num n => AccountScheme (GeneralLedger (Transaction n)) -> IncomeStatement n
buildIncomeStatement scheme =
  fmap (\(GeneralLedger acc xs) ->
          ( acc
          , (if assetOrLiability scheme $ code acc then id else negate) $ sum $ map transaction xs
          )
       ) scheme
