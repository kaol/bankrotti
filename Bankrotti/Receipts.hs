{-# LANGUAGE OverloadedStrings #-}

-- Combine all receipts into a single PDF using pdftk.  Add stamps
-- with the recepit id.
module Bankrotti.Receipts where

import Control.Concurrent (forkIO)
import Control.Concurrent.MVar
import Control.Exception (bracket_)
import Control.Monad
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import GHC.Conc (getNumProcessors)
import qualified Hledger as H
import System.Directory
import System.FilePath.Glob
import System.Process
import System.Unix.Directory
import Text.Printf

generateReceipts :: FilePath -> FilePath -> H.Journal -> IO ()
generateReceipts baseDir outputDir journal =
  withTemporaryDirectory "/tmp/bankrotti.XXXXXX" $ \tmpDir -> do
  let txs = zip [1..] $ filter (any ((== "kuitti") . fst) . H.ttags) . H.jtxns $ journal
      workDir = (tmpDir <>) . printf "work/%05i/" . fst
  cores <- getNumProcessors
  lock <- newEmptyMVar
  doneLock <- newEmptyMVar
  stampTemplate <- T.readFile "stamp.template"
  mapM_ (createDirectoryIfMissing True) $
    map (tmpDir <>) [ "sources", "stamp", "output" ] <>
    map workDir txs
  mapM_ (\tx -> forkIO $ bracket_ (takeMVar lock)
                (putMVar doneLock () >> putMVar lock ()) $
                generateReceipt stampTemplate baseDir (workDir tx) tx) txs
  -- Fork to handle the case where length txs < cores
  forkIO $ sequence_ $ replicate cores $ putMVar lock ()
  sequence_ $ replicate (length txs) $ takeMVar doneLock
  -- Drain the remaining concurrent locks
  sequence_ $ replicate cores $ takeMVar lock
  -- Merge all stamped files
  callProcess "pdftk" $
    (map ((<> "stamped.pdf") . workDir) $ zip [1..] txs) <>
    [ "cat", "output", outputDir <> "tositteet.pdf" ]

generateReceipt :: Text -> FilePath -> FilePath -> (Int, H.Transaction) -> IO ()
generateReceipt stampTemplate sourceDir workDir (idx, tx) = do
  let convertIfNeeded :: (Int, Text) -> IO Text
      convertIfNeeded (i, n) =
        if T.takeEnd 4 n == ".pdf"
        then return n
        else do
          let tgt = workDir <> printf "convert/%05i" i <> ".pdf"
          callProcess "convert" [ T.unpack n, "-background", "white", "-page", "a4", tgt ]
          return $ T.pack tgt
  createDirectoryIfMissing True $ workDir <> "convert/"
  sources <- mapM convertIfNeeded . zip [1..] . map T.pack =<<
    (concat <$>
     (mapM (glob . (sourceDir <>) . T.unpack) .
      map snd . filter ((== "kuitti") . fst) . H.ttags $ tx))
  T.writeFile (workDir <> "stamp.pdf") $ createStampFile (H.tcode tx) stampTemplate
  -- Merge sources
  callProcess "pdftk" $ map T.unpack sources <> [ "cat", "output", workDir <> "merged.pdf" ]
  -- Apply watermark
  callProcess "pdftk" [ workDir <> "merged.pdf"
                      , "stamp", workDir <> "stamp.pdf"
                      , "output", workDir <> "stamped.pdf"
                      ]

createStampFile :: Text -> Text -> Text
createStampFile code =
  -- TODO I tried to make a hollow stamp with outline but this is
  -- solid black.
  let pre = "BT /F1 40 Tf 10 800 TD 2 Tr 0.2 w 1 1 1 RG 0 0 0 rg ("
      post = ") Tj ET"
      stream = pre <> code <> post
      len = T.length stream
  in T.replace "$LEN" (T.pack $ show len) . T.replace "$STREAM" stream
