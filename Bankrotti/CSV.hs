{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}

module Bankrotti.CSV where

import Control.Applicative
import Control.Monad
import Data.ByteString.Lazy (ByteString)
import Data.Csv (toRecord)
import Data.Csv.Incremental
import Data.Decimal (Decimal, roundTo)
import Data.Foldable
import Data.Maybe (fromMaybe)
import Data.Time
import Data.Text (Text)
import qualified Data.Text as T
import qualified Hledger as H

import Bankrotti.AccountScheme
import Bankrotti.BalanceSheet
import Bankrotti.GeneralLedger
import Bankrotti.IncomeStatement

printAmount :: Decimal -> String
printAmount = show . roundTo 2

balanceSheetTotal addTo f =
  printAmount . ((+) <$> addTo <*> sum . map (\(Balance (_,b)) -> b) . f)

balanceSheetCSV :: BalanceSheet Decimal (Balance Decimal) -> ByteString
balanceSheetCSV = encode . flip foldMap
  [ textRow "Vastaavaa"
  , foldMap row . Bankrotti.BalanceSheet.assets
  , total "Vastaavaa" (const 0) Bankrotti.BalanceSheet.assets
  , textRow ""
  , textRow "Vastattavaa"
  , foldMap row . Bankrotti.BalanceSheet.liabilities
  , yearResult
  , total "Vastattavaa" (negate . resultBeforeAssetsAndLiabilities) Bankrotti.BalanceSheet.liabilities
  ] . flip ($)
  where
    textRow :: Text -> a -> Builder (Text, Text, Text, String)
    textRow t = const $ encodeRecord ( t, "", "", "" )
    row (Balance (_, 0)) = mempty
    row (Balance (acc, n)) = encodeRecord
      ( "", code acc, desc acc, printAmount n )
    total n addTo f = encodeRecord .
      ( "Yhteensä " <> n, "", "" , ) . balanceSheetTotal addTo f
    yearResult balance =
      let res = resultBeforeAssetsAndLiabilities balance
      in encodeRecord ( "", "Tilikauden tulos", "", printAmount $ negate res )

balanceSheetExpandedCSV :: [(Account, [Transaction H.Posting])] -> BalanceSheet Decimal (Balance Decimal) -> ByteString
balanceSheetExpandedCSV accountPostings = encode . flip foldMap
  [ textRow "Vastaavaa"
  , foldMap row . Bankrotti.BalanceSheet.assets
  , total "Vastaavaa" (const 0) Bankrotti.BalanceSheet.assets
  , textRow ""
  , textRow "Vastattavaa"
  , foldMap row . Bankrotti.BalanceSheet.liabilities
  , yearResult
  , total "Vastattavaa" (negate . resultBeforeAssetsAndLiabilities) Bankrotti.BalanceSheet.liabilities
  ] . flip ($)
  where
    textRow :: Text -> a -> Builder (Text, Text, Text, String, String)
    textRow t = const $ encodeRecord ( t, "", "", "", "" )
    row (Balance (acc, n)) = maybe mempty mconcat $ do
      guard (n > 0 && not (null accountPostings))
      return $
        [ encodeRecord ( "", code acc, desc acc, printAmount n, "" )
        , foldMap (foldMap encodePosting) $ lookup acc accountPostings
        ]
    total n addTo f = encodeRecord .
      (\x -> ( "Yhteensä " <> n, "", "", x, "" )) . balanceSheetTotal addTo f
    yearResult balance =
      let res = resultBeforeAssetsAndLiabilities balance
      in encodeRecord ( "", "Tilikauden tulos", "", printAmount $ negate res, "" )
    encodePosting tx = let posting = transaction tx in encodeRecord
      ( "", "", transactionDescription tx <> (maybe "" (" / " <>) $ postingDescription posting), ""
      , printAmount $ abs $ postingAmount posting
      )

type IncomeStatementOutputRow = (Text, Text, Text, Text, String)

type IncomeStatementAccum = ([Category (Account, Decimal)], Builder IncomeStatementOutputRow)

incomeStatementCSV :: IncomeStatement Decimal -> ByteString
incomeStatementCSV =  encode .
  ((<>)
   <$> snd . foldl encodeCategory ([], mempty)
   <*> profitLoss . sum . fmap (sum . fmap snd)) . categories
  where
    relax :: Builder IncomeStatementOutputRow
    relax = encodeRecord ( "", "", "", "", "" )
    encodeCategory :: IncomeStatementAccum -> Category (Account, Decimal) -> IncomeStatementAccum
    encodeCategory accum
      (TreeCategory { treeCategoryIncome = []
                    , treeCategoryExpenses = []}) = accum
    encodeCategory (xs, builder)
      tree@(TreeCategory { categoryName = name
                         , treeCategoryIncome = rs
                         , treeCategoryExpenses = es}) =
      ( tree:xs
      , builder <> (maybe mempty (subSum xs) (categorySubSum tree)) <> mconcat
        [ encodeRecord ( name, "", "", "", "" )
        , encodeLeafGroup "Tuotot" rs
        , relax
        , encodeLeafGroup "Kulut" es
        , relax
        , encodeTotal name (join $ fmap toList rs) (join $ fmap toList es)
        , relax
        ]
      )
    encodeCategory accum
      (SimpleCategory { simpleCategoryIncome = [], simpleCategoryExpenses = [] }) = accum
    encodeCategory (xs, builder)
      simple@(SimpleCategory { categoryName = name
                             , simpleCategoryIncome = rs
                             , simpleCategoryExpenses = es}) =
      ( simple:xs
      , builder <> (maybe mempty ((<>relax) . subSum xs) (categorySubSum simple)) <> mconcat
        [ encodeRecord ( name, "", "", "", "" )
        , encodeSimple "Tuotot" rs
        , relax
        , encodeSimple "Kulut" es
        , relax
        , encodeTotal name rs es
        , relax
        ]
      )
    subSum :: [Category (Account, Decimal)] -> Text -> Builder IncomeStatementOutputRow
    subSum xs name = encodeRecord $
      ( name, "", "", "", ) $ printAmount $ sum $ fmap (sum . fmap snd) xs
    encodeTotal :: Text -> [(Account, Decimal)] -> [(Account, Decimal)] -> Builder IncomeStatementOutputRow
    encodeTotal name rs es =
      encodeRecord ( "Yht. " <> name, "", "", ""
                   , printAmount $ sum . fmap snd $ rs <> es
                   )
    encodeLeafGroup :: Text -> [LeafCategory (Account, Decimal)] -> Builder IncomeStatementOutputRow
    encodeLeafGroup groupDesc xs = mconcat
      [ encodeRecord ( "", groupDesc, "", "", "" )
      , foldMap encodeLeaf xs
      , encodeRecord ("", "Yht. " <> groupDesc, "", ""
                     , printAmount $ sum $ (sum . fmap snd) <$> xs
                     )
      ]
    encodeLeaf :: LeafCategory (Account, Decimal) -> Builder IncomeStatementOutputRow
    encodeLeaf (LeafCategory name xs) = mconcat
      [ encodeRecord ( "", "", name, "", "" )
      , foldMap (\(acc, i) -> encodeRecord
                  ( "", "", "", accountFullName acc, printAmount i)
                ) xs
      , encodeRecord ( "", "", "Yht. " <> name, ""
                     , printAmount . sum $ snd <$> xs
                     )
      , relax
      ]
    encodeSimple groupDesc xs = mconcat
      [ encodeRecord ( "", groupDesc, "", "", "" )
      , foldMap (\(acc, i) -> encodeRecord
                  ( "", "", accountFullName acc, "", printAmount i)
                ) xs
      , encodeRecord ( "", "Yht. " <> groupDesc, "", ""
                     , printAmount $ sum $ snd <$> xs
                     )
      ]
    profitLoss :: Decimal -> Builder IncomeStatementOutputRow
    profitLoss = encodeRecord .
      ( "Tilikauden tulos", "", "", "", ) . printAmount

journalCSV :: Journal -> ByteString
journalCSV = encode . mconcat . map encodeRecord . transactions

cumulativeCSV :: Journal -> ByteString
cumulativeCSV journal = encode $ foldMap (encodeRecord . row) cumulative
  where
    cumulative =
      map (\x@(t, _) -> zipWith (*>) (transaction t) . map Just <$> x) .
      tail . scanl (\cs t -> ( t
                             , zipWith (+) (snd cs) $ map (fromMaybe 0) $ transaction t)
                   )
      (undefined, (replicate (length $ journalAccounts journal) 0) :: [Decimal]) $
      transactions journal
    row (t, xs) =
      [ formatTime defaultTimeLocale "%F" $ transactionDay t
      , T.unpack $ transactionCode t
      , T.unpack $ transactionDescription t
      ]
      <> map (maybe "" printAmount) (transaction t)
      <> map (maybe "" printAmount) xs

fullJournalCSV :: [Transaction [(Account, H.Posting)]] -> ByteString
fullJournalCSV journal = encode $ mconcat
  [ encodeRecord ("pvm", "tosite", "tapahtuma", "kirjaus", "tili", "tili selite", "debet", "kredit")
  , foldMap encodeOne journal
  ]
  where
    encodeOne tx = mconcat
      [ encodeRecord ( formatTime defaultTimeLocale "%F" $ transactionDay tx
                     , transactionCode tx
                     , transactionDescription tx
                     , "", "", "", "", ""
                     )
      , foldMap encodeRow $ transaction tx
      , encodeRecord ( "", "", "", "", "", "", "", "" )
      ]
    encodeRow (acc, posting) = let s = postingAmount posting
                               in encodeRecord
      ( "", "", ""
      , fromMaybe "" $ postingDescription posting
      , code acc
      , desc acc
      , if s > 0 then printAmount s else ""
      , if s < 0 then printAmount $ negate s else ""
      )

type GeneralLedgerRow = (Text, Text, String, String, String, String, String, String, String, String)

generalLedgerCSV :: AccountScheme (GeneralLedger (Transaction H.Posting)) -> ByteString
generalLedgerCSV journal = encode $ mconcat
  [ encodeRecord ("", "", "pvm", "", "tosite", "tapahtuma", "kirjaus", "debet", "kredit", "saldo")
  , foldMap encodeOne filtered
  , totals $ (fmap . fmap . fmap) postingAmount filtered
  ]
  where
    filtered = filterAssetsAndLiabilities journal

    encodeOne :: GeneralLedger (Transaction H.Posting) -> Builder GeneralLedgerRow
    encodeOne (GeneralLedger axx []) = mempty
    encodeOne (GeneralLedger acc tx) = mconcat
      [ encodeRecord ( code acc, desc acc, "", "", "", "", "", "", "", "" )
      , foldMap (\(x,cum) -> let tAmount = postingAmount $ transaction x
                                 tAmountAbs = abs tAmount in encodeRecord
                  ( "", ""
                  , formatTime defaultTimeLocale "%F" $ transactionDay x, ""
                  , T.unpack $ transactionCode x
                  , T.unpack $ transactionDescription x
                  , maybe "" T.unpack . postingDescription $ transaction x
                  , if tAmount > 0 then printAmount tAmountAbs else ""
                  , if tAmount < 0 then printAmount tAmountAbs else ""
                  , printAmount cum
                  )) $ zip tx' $ (tail $ scanl (+) 0 $ map (postingAmount . transaction) tx')
      , let ss = map (postingAmount . transaction) tx in
        encodeRecord ( "", "", "", "Yhteensä " <> show (length tx) <> " kpl", "", "", ""
                     , printAmount . sum . filter (>0) $ ss
                     , printAmount . abs . sum . filter (<0) $ ss
                     , ""
                     )
      , encodeRecord ( "", "", "", "Loppusaldo", "", "", "", "", ""
                     , printAmount . sum $ map (postingAmount . transaction) tx
                     )
      , encodeRecord ( "", "", "", "", "", "", "", "", "", "" )
      ]
      where
        tx' = reverse tx
    totals scheme =
      let tx = (toList <=< toList <=< toList) scheme
      in encodeRecord ( "", "", "", "Yhteensä", "", "", ""
                      , show . sum $ filter (>0) tx
                      , show . abs . sum $ filter (<0) tx
                      , ""
                      )
