{-# LANGUAGE DeriveFoldable, DeriveFunctor #-}

module Bankrotti.BalanceSheet where

import Control.Monad
import Data.Foldable
import Data.Monoid

import Bankrotti.AccountScheme
import Bankrotti.GeneralLedger

data BalanceSheet n a = BalanceSheet
  { assets :: [a]
  , liabilities :: [a]
  , resultBeforeAssetsAndLiabilities :: n
  , result :: n
  }
  deriving (Show, Foldable, Functor)

newtype Balance n = Balance (Account, n)
  deriving (Functor)

balanceSheet :: Num n => AccountScheme (GeneralLedger (Transaction n)) -> BalanceSheet n (Balance n)
balanceSheet = BalanceSheet
  <$> map toBalance . Bankrotti.AccountScheme.assets
  <*> map (fmap negate . toBalance) . Bankrotti.AccountScheme.liabilities
  <*> yearTotal . Bankrotti.AccountScheme.categories . filterAssetsAndLiabilities
  <*> yearTotal . Bankrotti.AccountScheme.categories
  where
    toBalance (GeneralLedger acc xs) = Balance (acc, sum $ map transaction xs)
    yearTotal :: Num n => [Category (GeneralLedger (Transaction n))] -> n
    yearTotal = getSum . (foldMap . foldMap . foldMap . foldMap) Sum
