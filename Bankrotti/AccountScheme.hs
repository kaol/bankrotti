{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveFoldable, DeriveFunctor #-}
{-# LANGUAGE FlexibleInstances #-}

module Bankrotti.AccountScheme where

import Data.Text (Text)
import qualified Data.Text as T
import Text.XML.HXT.Core
import Text.XML.HXT.Arrow.Pickle

data AccountScheme a = AccountScheme
  { assets :: [a]
  , liabilities :: [a]
  , categories :: [Category a]
  }
  deriving (Show, Foldable, Functor)

data Category a
  = TreeCategory
    { categoryName :: Text
    , categoryMandatory :: Bool
    , categorySubSum :: Maybe Text
    , treeCategoryIncome :: [LeafCategory a]
    , treeCategoryExpenses :: [LeafCategory a]
    }
  | SimpleCategory
    { categoryName :: Text
    , categoryMandatory :: Bool
    , categorySubSum :: Maybe Text
    , simpleCategoryIncome :: [a]
    , simpleCategoryExpenses :: [a]
    }
  deriving (Show, Foldable, Functor)

data LeafCategory a = LeafCategory Text [a]
  deriving (Show, Foldable, Functor)

data Account = Account
  { code :: Text
  , desc :: Text
  , journalAccount :: Bool
  }
  deriving (Show, Eq, Ord)

accountFullName :: Account -> Text
accountFullName = flip foldMap [code, const " ", desc] . flip ($)

instance XmlPickler (AccountScheme Account) where
  xpickle = xpAccountScheme

xpAccountScheme = xpElem "tilikartta" $
  xpWrap ( \(a, b, c) -> AccountScheme a b c
         , \(AccountScheme a b c) -> (a, b, c)
         ) $
  xpTriple
  (xpElem "vastaavaa" $ xpList xpAccount)
  (xpElem "vastattavaa" $ xpList xpAccount)
  (xpElem "tilit" $ xpList xpCategories)
  where
    xpAttrBool n = xpWrap ( (== Just 1)
                          , \x -> if x then Just 1 else Nothing
                          ) $ xpAttrImplied n xpPrim
    xpText' = xpWrap (T.pack, T.unpack) xpText
    xpAccount =
      xpElem "tili" $
      xpWrap ( \(a,b,c) -> Account a b c
             , \(Account a b c) -> (a, b, c)
             ) $
      xpTriple
      (xpAttr "id" xpText')
      xpText'
      (xpAttrBool "päiväkirja")
    numCat (TreeCategory {}) = 0
    numCat _ = 1
    xpCategories = xpAlt numCat
      [ xpElem "pääkategoria" $
        xpWrap ( \(a, b, c, d, e) -> TreeCategory a b c d e
               , \(TreeCategory a b c d e) -> (a, b, c, d, e)
               ) $
        xp5Tuple
        (xpAttr "nimi" xpText')
        (xpAttrBool "aina")
        (xpOption $ xpElem "välisumma" xpText')
        (xpElem "tulot" $ xpList xpLeafCategory)
        (xpElem "menot" $ xpList xpLeafCategory)
      , xpElem "kategoria" $
        xpWrap ( \(a, b, c, d, e) -> SimpleCategory a b c d e
               , \(SimpleCategory a b c d e) -> (a, b, c, d, e)
               ) $
        xp5Tuple
        (xpAttr "nimi" xpText')
        (xpAttrBool "aina")
        (xpOption $ xpElem "välisumma" xpText')
        (xpElem "tulot" $ xpList xpAccount)
        (xpElem "menot" $ xpList xpAccount)
      ]
    xpLeafCategory = xpElem "kategoria" $
      xpWrap ( \(a, b) -> LeafCategory a b
             , \(LeafCategory a b) -> (a, b)
             ) $
      xpPair
      (xpAttr "nimi" xpText')
      (xpList xpAccount)

readFile :: FilePath -> IO (AccountScheme Account)
readFile = fmap head . runX . xunpickleDocument xpAccountScheme [withRemoveWS yes, withValidate no]
