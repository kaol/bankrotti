{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleInstances #-}

module Bankrotti.GeneralLedger where

import Control.Applicative
import Control.Monad
import Data.Csv
import Data.Decimal (Decimal)
import Data.Foldable
import Data.List
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Monoid
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import Data.Time
import qualified Hledger as H

import Bankrotti.AccountScheme

data GeneralLedger a = GeneralLedger Account [a]
  deriving (Show, Foldable, Functor)

assetOrLiability :: AccountScheme (GeneralLedger a) -> Text -> Bool
assetOrLiability scheme =
  flip elem $ map (\(GeneralLedger acc _) -> code acc) $ assets scheme <> liabilities scheme

filterAssetsAndLiabilities :: AccountScheme (GeneralLedger a) -> AccountScheme (GeneralLedger a)
filterAssetsAndLiabilities scheme =
  scheme { categories = map filterTree $ categories scheme }
  where
    isAssetOrLiability = assetOrLiability scheme . (\(GeneralLedger acc _) -> code acc)
    filterTree tree@(TreeCategory {}) =
      tree { treeCategoryIncome = map filterLeafs $ treeCategoryIncome tree
           , treeCategoryExpenses = map filterLeafs $ treeCategoryExpenses tree
           }
    filterTree simple@(SimpleCategory {}) =
      simple { simpleCategoryIncome = filter (not . isAssetOrLiability) $ simpleCategoryIncome simple
             , simpleCategoryExpenses = filter (not . isAssetOrLiability) $ simpleCategoryExpenses simple
             }
    filterLeafs (LeafCategory n xs) =
      LeafCategory n $ filter (not . isAssetOrLiability) xs

data Transaction a = Transaction
  { transactionDay :: Day
  , transactionCode :: Text
  , transactionDescription :: Text
  , transaction :: a
  } deriving (Show, Foldable, Functor, Traversable)

instance Applicative Transaction where
  pure = Transaction (fromGregorian 1970 1 1) "" ""
  a <*> b =
    let res = transaction a $ transaction b in
    if T.null $ transactionCode a
    then b { transaction = res }
    else a { transaction = res }

data Journal = Journal
  { journalAccounts :: [Account]
  , transactions :: [Transaction [Maybe Decimal]]
  }
  deriving (Show)

instance ToRecord (Transaction [Maybe Decimal]) where
  toRecord = record . join . flip map
    ((map (pure . )
      [ toField . formatTime defaultTimeLocale "%F" . transactionDay
      , toField . transactionCode
      , toField . transactionDescription
      ])
      <> ([map (toField . maybe "" show) . transaction])
    ) . flip ($)

isAccount :: Account -> H.AccountName -> Bool
isAccount acc = any (== (code acc)) . T.split (== ':')

matchTransactionToAccounts :: [Text] -> H.Transaction -> [Maybe (Text, Transaction H.Posting)]
matchTransactionToAccounts accountCodes t =
  let date = H.tdate t
      sourceDocumentId = H.tcode t
      description = H.tdescription t
      accountCode = asum . map (liftA2 (<*) pure $ guard . (`elem` accountCodes)) .
                    T.split (== ':') . H.paccount
  in map (fmap
          <$> (flip (,) . Transaction date sourceDocumentId description)
          <*> accountCode) $ H.tpostings t

postingAmount :: H.Posting -> Decimal
postingAmount = H.aquantity . fromJust . H.unifyMixedAmount . H.pamount

postingDescription :: H.Posting -> Maybe Text
postingDescription =
  ((>>) <$> guard . not . T.null <*> Just) . T.strip . T.takeWhileEnd (/= ',') . H.pcomment

accountTransactions :: [Text] -> H.Journal -> [(Text, Transaction H.Posting)]
accountTransactions accountCodes =
  catMaybes . concatMap (matchTransactionToAccounts accountCodes) . H.jtxns

buildLedgers :: AccountScheme Account -> H.Journal -> AccountScheme (GeneralLedger (Transaction H.Posting))
buildLedgers scheme journal =
  fmap (\acc ->
          GeneralLedger acc . maybe mempty id $ Map.lookup (code acc) transactionsMap) scheme
  where
    accountCodes = code <$> toList scheme
    transactionsMap :: Map Text [Transaction H.Posting]
    transactionsMap = Map.fromListWith
      (<>) $ fmap pure <$> accountTransactions accountCodes journal

buildJournal :: AccountScheme Account -> H.Journal -> Journal
buildJournal scheme =
  let acc = filter journalAccount $ toList scheme
      findAccountPosting :: Account -> H.Transaction -> Maybe (Transaction Decimal)
      findAccountPosting acc =
        fmap (fmap postingAmount . snd) .
        asum .
        matchTransactionToAccounts [code acc]
      accTransactions :: H.Journal -> [Transaction [Maybe Decimal]]
      accTransactions = fmap sequenceA . (fmap . fmap) sequenceA . filter (any isJust) .
        map (flip map acc . flip findAccountPosting) . H.jtxns
  in Journal acc . accTransactions

buildFullJournal :: AccountScheme Account -> H.Journal -> [Transaction [(Account, H.Posting)]]
buildFullJournal scheme journal = transactions
  where
    accountCodes = code <$> toList scheme
    accountMap = Map.fromList $ ((,) <$> code <*> id) <$> toList scheme
    transactions =
      map (\tx -> let sample = snd $ head tx
                  in sample { transaction = (fmap . fmap) transaction tx } ) .
      (fmap . fmap) (\(a,ts) -> (accountMap Map.! a, ts)) .
      map (catMaybes . matchTransactionToAccounts accountCodes) $ H.jtxns journal

filterSheetPostings :: (Eq a, Ord a) => [Transaction [(a, H.Posting)]] -> [(a, [Transaction H.Posting])]
filterSheetPostings =
  map (\(a, b) -> (appEndo a undefined, b)) .
  map sequenceA .
  (fmap . fmap) (\(a,b) -> (Endo $ const a, b)) .
  groupBy (byFst (==)) . sortBy (byFst compare) .
  map ((,) <$> fst . transaction <*> fmap snd) .
  join . map sequenceA .
  (fmap . fmap) (filter (isSheetPosting . snd))
  where
    isSheetPosting :: H.Posting -> Bool
    isSheetPosting = elem ("tase","1") . H.ptags
    byFst f a b = f (fst a) (fst b)
